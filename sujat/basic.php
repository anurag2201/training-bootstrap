<!DOCTYPE html>
<html>
    <head>
        <style>
            body {
               background-image: url("paper.jpg");
               background-repeat: repeat-x;
               background-position: right top;
               background-attachment: fixed;
            }

            h1 {
                color: white;
                text-align: center;
                 border-radius: 5px;
            }
            h3 {
                color: black;
                text-align: center;
            }

            p {
                font-family: verdana;
                font-size: 14px;

                color: red;
                text-align: center ;
            }
            #p1 {
                font-family:Times New Roman;
                text-align: center;
                color: blue;
            }
            #p2 {
                 font-family:sans-serif;
                text-align: center;
                color: green;
            }
            #p3 {
                 font-family:Helvetica;
                text-align: center;
                color: greenyellows;
            }
        </style>
    </head>
    <body>

        <h1 style="background-color:Tomato;border:2px solid black;">Hello all friends</h1>
        <h3>Hello Sujat</h3>

        <p id="p1">Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <h3>Hello Ahmad</h3>

        <p id="p2">Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <h3>Hello Malik</h3>
        <p id="p3">Paragraph Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
   <h1 style="color:red;margin-left:30px;background-color:gray;border:2px solid black;">"Okay"</h1>
    </body>
</html>