$(document).ready(function () {

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });

    $("body").on("click", ".btn-box-show", function () {
        if ($(this).parent(".box-example-item").hasClass("open")) {
            $(this).parent(".box-example-item").removeClass("open");
        } else {
            $(this).parent(".box-example-item").addClass("open");
        }

    });
    $("body").on("click", ".btn-show-anywhere", function () {
        if ($($(this).attr("data-target")).css("display") == "none") {
            $($(this).attr("data-target")).show(700);
        } else {
            $($(this).attr("data-target")).hide(700);
        }
//        if ($($(this).attr("data-target")).hasClass("open")) {
//            $($(this).attr("data-target")).removeClass("open");
//        } else {
//            $($(this).attr("data-target")).addClass("open");
//        }

    });
});