<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Home | Larva Training</title>
        <link href="libs/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
        <link href="theme/theme.css" type="text/css" rel="stylesheet" />
        <!--<link href="libs/bootstrap/css/custom-theme.css" type="text/css" rel="stylesheet" />-->
        <script src="libs/jquery.js" type="text/javascript"></script>
        <script src="libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/index.js" type="text/javascript"></script>
        <style type="text/css">
            h1#page-title {
                color: orange;
            }
        </style>
    </head>
    <body>
        <?php include './menu.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="list-group">
                        <a href="#" class="list-group-item active">
                            Bootstrap Landing
                        </a>
                        <a href="sujat/bootstrap/index.php" class="list-group-item">Sujat</a>
                        <a href="#" class="list-group-item">Morbi leo risus</a>
                        <a href="#" class="list-group-item">Porta ac consectetur ac</a>
                        <a href="#" class="list-group-item">Vestibulum at eros</a>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-4">.col-md-4</div>
                <div class="col-md-4 col-md-offset-1">.col-md-4 .col-md-offset-4</div>
            </div>

            <div class="row">
                <div class="col-xs-6 col-sm-4">
                    col-xs-6 col-sm-4
                </div>
                <div class="col-xs-6 col-sm-4">
                    col-xs-6 col-sm-4
                </div>
                <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0">
                    col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    Level 1: .col-sm-9
                    <div class="row">
                        <div class="col-xs-8 col-sm-6">
                            Level 2: .col-xs-8 .col-sm-6
                        </div>
                        <div class="col-xs-4 col-sm-6">
                            Level 2: .col-xs-4 .col-sm-6
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9 col-md-push-3">.col-md-9 .col-md-push-3</div>
                <div class="col-md-3 col-md-pull-9">.col-md-3 .col-md-pull-9</div>
            </div>
            <p class="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.</p>

            <p>You can use the mark tag to <mark>highlight</mark> text.</p>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p class="text-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <!--<p class="text-nowrap">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>-->
            <div class="row">
                <div class="col-md-4">
                    <span>Press Me</span>
                    <span>Press Me</span>
                    <span>Press Me</span>
                    <span>Press Me</span>
                    <span>Press Me</span>
                    <span>Press Me</span>
                    <span>Press Me</span>

                    <abbr title="attribute">attr</abbr>
                </div>
                <div class="col-md-6">
                    <blockquote>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                        <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                    </blockquote>
                    <a href="http://google.com" class="btn btn-success btn-danger btn-warning btn-default btn-info btn-sm">Go to</a>

                    <pre>&lt;p&gt;Sample text here...&lt;/p&gt;</pre>
                </div>

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Sex</th>
                                    <th>Age</th>
                                    <th>Age</th>
                                    <th>Age</th>
                                    <th>Age</th>
                                    <th>Age</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="success">
                                    <td>Anurag asjfalkdsjf askljfl aldfjklajdklf klasj fkldskljfskla dklfjsk</td>
                                    <td>Male afjkldsfklaskldfjkl</td>
                                    <td>29</td>
                                    <td>29</td>
                                    <td>29</td>
                                    <td>29</td>
                                    <td>29</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                
                <p>
                    
                </p>


            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4>Sign Up Form</h4>
                        </div>
                        <div class="panel-body">
                            <form>
                                <div class="form-group has-warning">
                                    <label for="email">Email address:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></div>
                                        <div class="form-control form-control-static">anurag2201@gmail.com</div>
                                    </div>
                                </div>
                                <div class="form-group has-feedback">
                                    <label for="email" class="control-label">Email address:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></div>
                                        <input id="email" type="text" class="form-control" id="exampleInputAmount" placeholder="Email address" readonly>

                                    </div>
                                    <span class="form-control-feedback glyphicon glyphicon-ok"></span>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Tell us about yourself..." rows="5" style="resize: none;"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox-inline disabled"><label><input type="checkbox" value="Accept" disabled/> 1</label></div>
                                    <div class="checkbox-inline"><label><input type="checkbox" value="Accept"/> 2</label></div>
                                </div>
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-ok-circle"></i> Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <h4 class="visible-print-block">Hello</h4>
                <div class="col-md-4">
                    <div class="thumbnail" style="position: relative;">
                        <button type="button" class="close" aria-label="Close" style="position: absolute;top: 10px;right: 10px;"><span aria-hidden="true">&times;</span></button>
                        <img src="http://s3.amazonaws.com/finecooking.s3.tauntonclud.com/app/uploads/2017/04/18203548/fc81wa064-01-thumb1x1.jpg" class="img-responsive" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div id="box1" class="box-example-item">
                        <h1>BOX 1</h1>
                        <div class="box-pop-up">
                            <ul>
                                <li><a href="#">Option 1</a></li>
                                <li><a href="#">Option 2</a></li>
                                <li><a href="#">Option 3</a></li>
                                <li><a href="#">Option 4</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-primary btn-box-show">Show Options</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-example-item">
                        <div class="box-pop-up">
                            <ul>
                                <li><a href="#">Option 1</a></li>
                                <li><a href="#">Option 2</a></li>
                                <li><a href="#">Option 3</a></li>
                                <li><a href="#">Option 4</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-primary btn-box-show" data-target="5">Show Options</button>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-example-item">
                        <div class="box-pop-up">
                            <ul>
                                <li><a href="#">Option 1</a></li>
                                <li><a href="#">Option 2</a></li>
                                <li><a href="#">Option 3</a></li>
                                <li><a href="#">Option 4</a></li>
                            </ul>
                        </div>
                        <button class="btn btn-primary btn-box-show">Show Options</button>
                    </div>
                </div>
            </div>

            <button class="btn btn-primary btn-show-anywhere" data-target="#box1" data-toggle="tooltip" data-placement="left" title="Tooltip on left">Show View BOX 1</button>

            <div class="dropdown">
                <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown trigger
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li><a href="#">Option 1</a></li>
                    <li><a href="#">Option 1</a></li>
                    <li><a href="#">Option 1</a></li>
                    <li><a href="#">Option 1</a></li>
                </ul>
            </div>

            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="home">
                        <h1>Home container</h1>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <h1>Profile container</h1>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="messages">
                        <h1>Messages container</h1>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="settings">
                        <h1>Settings container</h1>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div id="accordion" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Collapsible Group Item #1
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                        Collapsible Group Item #1
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h1 id="page-title" data-toggle="tooltip" data-placement="top" title="Tooltip on top">Page Title Here</h1>
        <button class="btn btn-primary"><i class="glyphicon glyphicon-alert"></i> Click Me!!!</button>
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Dropdown
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </div>

        <ul class="list-group">
            <li class="list-group-item-heading">HTML</li>
            <li class="list-group-item"><a href="training/positions-float.html">Positions/Float</a></li>
        </ul>

        <div class="text-center">
            <button class="btn btn-danger" data-toggle="popover" data-placement="top" title="Popover title" data-content="And here's some amazing content. It's very engaging. Right?">Popover</button>
        </div>

        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4>Oops no result found</h4>
            <ul>
                <li>Try any other keyword</li>
                <li>Go to home page</li>
            </ul>
        </div>

        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-primary active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked> Radio 1 (preselected)
            </label>
            <label class="btn btn-success">
                <input type="radio" name="options" id="option2" autocomplete="off"> Radio 2
            </label>
            <label class="btn btn-primary">
                <input type="radio" name="options" id="option3" autocomplete="off"> Radio 3
            </label>
        </div>

        <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Link with href
        </a>
        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Button with data-target
        </button>
        <div class="collapse" id="collapseExample">
            <div class="well">
                ...
            </div>
        </div>
    </body> 
</html>
