/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $("p").click(function(){
        $(this).hide();
    });
    $("#hide").click(function(){
        $("#pragraf").hide();
    });
    $("#show").click(function(){
        $("#pragraf").show();
    });
    $("#hide2").click(function(){
        $("#pragraf1").hide(1000);
    });
    $("#show2").click(function(){
        $("#pragraf1").show(1000);
    });
    $("#button").click(function(){
        $("#pragraf3").toggle(500);
        $("#pragraf4").toggle(800);
    });
    $("#btn-fadin").click(function(){
        $("#div1").fadeIn();
        $("#div2").fadeIn("slow");
        $("#div3").fadeIn(3000);
    });
    $("#btn-fadout").click(function(){
        $("#div1").fadeOut();
        $("#div2").fadeOut("slow");
        $("#div3").fadeOut(3000);
    });
    
    $("#btn-toggle").click(function(){
        $("#tdiv1").fadeToggle();
        $("#tdiv2").fadeToggle("slow");
        $("#tdiv3").fadeToggle(3000);
    });
    $("#btn-to").click(function(){
    $("#todiv1").fadeTo("slow", 0.15);
    $("#todiv2").fadeTo("slow", 0.4);
    $("#todiv3").fadeTo("slow", 0.7);
    });
    $(function(){
       $(".panel-body").hide();
    });
    $(".panel-heading").click(function(){
    $(".panel-body").slideToggle();
    });
    $("#btn-animate").click(function(){
       $("#anidiv").animate({left: '250px'});
    });
});

