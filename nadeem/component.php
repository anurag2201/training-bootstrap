<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Learning|component page</title>
    </head>
<body>
    <?php include("menu.php"); ?>
    <div class = 'container'>
        <h3>Glyp Icon</h3>
        <div class='row'>
            <div class = 'col-xs-12 col-sm-10 col-md-8 col-lg-6'>
                <div class="alert alert-danger" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Enter a valid email address
                </div>
            </div>
        </div> 
        <div class='row'>
            <div class = 'col-xs-12 col-sm-10 col-md-8 col-lg-6'>
                <div class="alert alert-success" role="alert">
                <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
                nadeem@gmail.com
                </div>
            </div>
        </div> 
        <h3>Dropdowns list</h3>
      
            <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Select
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
        <h3>Button Groups</h3>
        <div class='row'>
        <div class="btn-group" role="group" aria-label="...">
        <button type="button" class="btn btn-default">Left</button>
        <button type="button" class="btn btn-default">Middle</button>
        <button type="button" class="btn btn-default">Right</button>
        </div>
        </div>
        <div class='row' style="margin-top: 10px;">
        <div class="btn-toolbar" role="toolbar" aria-label="...">
            <div class="btn-group" role="group" aria-label="...">
               <button type="button" class="btn btn-default">1</button>
                <button type="button" class="btn btn-default">2</button>
                <button type="button" class="btn btn-default">3</button> 
                <button type="button" class="btn btn-default">4</button> 
            </div>
            <div class="btn-group" role="group" aria-label="...">
                <button type="button" class="btn btn-default">0</button>
                <button type="button" class="btn btn-default">1</button>
                <button type="button" class="btn btn-default">2</button>
            </div>
            <div class="btn-group" role="group" aria-label="...">
                <button type="button" class="btn btn-default">1</button>
            </div>
        </div>
        </div>
        <h3>Button dropdowns</h3>
        <!-- Single button -->
        <div class="btn-group">
          <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class='row' style='margin-top: 5px;'>
        <!-- Split button -->
        <div class="btn-group">
          <button type="button" class="btn btn-danger">Action</button>
          <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-warning">Action</button>
          <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-success">Action</button>
          <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <div class="btn-group">
          <button type="button" class="btn btn-info">Action</button>
          <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        </div>
        <h3>Inputs group</h3>
            <div class="input-group" style='margin-top: 5px;'>
              <span class="input-group-addon" id="basic-addon1">@</span>
              <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
            </div>

            <div class="input-group" style='margin-top: 5px;'>
              <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
              <span class="input-group-addon" id="basic-addon2">@example.com</span>
            </div>

            <div class="input-group" style='margin-top: 5px;'>
              <span class="input-group-addon">$</span>
              <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
              <span class="input-group-addon">.00</span>
            </div>

            <label for="basic-url" style='margin-top: 5px;'>Your vanity URL</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
              <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
            </div>
            <h3>Navigation</h3>
            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="">Home</a></li>
                <li role="presentation"><a href="">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" ><a href="">Home</a></li>
                <li role="presentation" class="active"><a href="">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <ul class="nav nav-pills">
                <li role="presentation" class="active"><a href="">Home</a></li>
                <li role="presentation"><a href="">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation" class="active"><a href="">Home</a></li>
                <li role="presentation"><a href="">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <ul class="nav nav-pills nav-justified">
                <li role="presentation" class="active"><a href="">Home</a></li>
                <li role="presentation"><a href="">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <ul class="nav nav-pills">
                <li role="presentation" class="active"><a href="">Home</a></li>
                <li role="presentation" class="disabled"><a href="#">Profile</a></li>
                <li role="presentation"><a href="">Messages</a></li>
            </ul>
            <h3>Pagination</h3>
                <nav aria-label="Page navigation">
                <ul class="pagination">
                <li>
                  <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
                </ul>
                </nav>
            <nav aria-label="...">
            <ul class="pagination">
              <li class="disabled">
                <span>
                  <span aria-hidden="true">&laquo;</span>
                </span>
              </li>
              <li class="active">
                <span>1 <span class="sr-only">(current)</span></span>
              </li>
              <li>
                <span>2<span class="sr-only">(current)</span></span>
              </li>
              <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
            </ul>
          </nav>
            <h3>Pager</h3>
            <nav aria-label="...">
            <ul class="pager">
              <li><a href="#">Previous</a></li>
              <li><a href="#">Next</a></li>
            </ul>
            </nav>
            <h3>side link</h3>
            <nav aria-label="...">
            <ul class="pager">
                <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
            </ul>
            </nav>
            <nav aria-label="...">
            <ul class="pager">
                <li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
            </ul>
            </nav>
            <h3>Labels</h3>
            <h6>Example heading <span class="label label-default">New</span></h6>
            <h5>Example heading <span class="label label-danger">New</span></h5>
            <h4>Example heading <span class="label label-success">New</span></h4>
            <h3>Example heading <span class="label label-warning">New</span></h3>
            <h2>Example heading <span class="label label-primary">New</span></h2>
            <h1>Example heading <span class="label label-info">New</span></h1>
            <h3>variations</h3>
            <span class="label label-default">Default</span>
            <span class="label label-primary">Primary</span>
            <span class="label label-success">Success</span>
            <span class="label label-info">Info</span>
            <span class="label label-warning">Warning</span>
            <span class="label label-danger">Danger</span>
            <h3>Badges</h3>
            <a href="#">Inbox <span class="badge">42</span></a>
            <button class="btn btn-primary" type="button">
              Messages <span class="badge">4</span>
            </button>
            <h3>Adapts to active nav states</h3>
            <ul class="nav nav-pills" role="tablist">
            <li role="presentation" class="active"><a href="#">Home <span class="badge">42</span></a></li>
            <li role="presentation"><a href="#">Profile</a></li>
            <li role="presentation"><a href="#">Messages <span class="badge">3</span></a></li>
            </ul>
            <h3>Jumbotron</h3>
            <div class="jumbotron">
            <h1>Hello, world!</h1>
            <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
            </div>
            <h3>Page header</h3>
            <div class="page-header">
            <h1>Example page header <small>Subtext for header</small></h1>
            </div>
            <h3>Thumbnails</h3>
            <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="images/yest5.jpg" class="thumbnail">
                    <div class="img-block" style="background-image: url(images/yest5.jpg);"></div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3">
                <a href="images/test1.jpg" class="thumbnail">
                    <div class="img-block" style="background-image: url(images/test1.jpg);"></div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3">
                <a href="images/test2.jpg" class="thumbnail">
                    <div class="img-block" style="background-image: url(images/test2.jpg);"></div>
                </a>
            </div>
            <div class="col-xs-6 col-md-3">
                <a href="images/test3.jpg" class="thumbnail">
                    <div class="img-block" style="background-image: url(images/test3.jpg);"></div>
                </a>
            </div>
            </div>
            <h3>Thumbnail label</h3>
            <div class="row">
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <div class="img-block" style="background-image: url(images/test1.jpg);"></div>
            <div class="caption">
            <h3>Thumbnail label</h3>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
            </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <div class="img-block" style="background-image: url(images/test1.jpg);"></div>
            <div class="caption">
            <h3>Thumbnail label</h3>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
            </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
            <div class="img-block" style="background-image: url(images/test1.jpg);"></div>
            <div class="caption">
            <h3>Thumbnail label</h3>
            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
            <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
            </div>
            </div>
            </div>
            </div>
            <h3>Alerts</h3>
            <div class="aler_row">
                <div class="alert alert-success" role="alert"><span style="font-weight: bold;">Well done!</span> You successfully read this important alert message.</div>
                <div class="alert alert-info" role="alert"><span style="font-weight: bold;">Heads up!</span> This alert needs your attention, but it's not super important.</div>
                <div class="alert alert-warning" role="alert"><span style="font-weight: bold;">Warning!</span> Better check yourself, you're not looking too good.</div>
                <div class="alert alert-danger" role="alert"><span style="font-weight: bold;">Oh snap!</span> Change a few things up and try submitting again.</div>
            </div>
            <h3>Dismissible alerts</h3>
            <div class="aler_row">
                <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> Better check yourself, you're not looking too good.
                </div>
            </div>
            <h3>Links in alerts</h3>
            <div class="aler_row">
                <div class="alert alert-success" role="alert">
                    <strong>Well done!</strong>
                    You successfully read 
                    <a href="#" class="alert-link"><strong>this important alert message.</strong></a>
                </div>
                <div class="alert alert-info" role="alert">
                  <a href="#" class="alert-link">eads up! This alert needs your attention, but it's not super important.</a>
                </div>
                <div class="alert alert-warning" role="alert">
                  <a href="#" class="alert-link">Warning! Better check yourself, you're not looking too good.</a>
                </div>
                <div class="alert alert-danger" role="alert">
                  <a href="#" class="alert-link">Oh snap! Change a few things up and try submitting again.</a>
                </div>
            </div>
            <h3>Progress bars</h3>
              <div class="aler_row">
                <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                    <span class="sr-only">60% Complete</span>
                </div>
                </div>
              </div>
            <h3></h3>
                <div class="aler_row">
                    <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                        60%
                    </div>
                    </div>
                </div>
                 <div class="aler_row">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">
                    0%
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: 2%;">
                    2%
                  </div>
                </div>
                </div>
            
            <h3></h3>
            <div class="aler_row">
                                <div class="progress">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                    <span class="sr-only">40% Complete (success)</span>
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                    <span class="sr-only">20% Complete</span>
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                    <span class="sr-only">60% Complete (warning)</span>
                  </div>
                </div>
                <div class="progress">
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                    <span class="sr-only">80% Complete (danger)</span>
                  </div>
                </div>
            </div>
            <h3></h3>
            <div class="aler_row">
                    <div class="progress">
                      <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                        <span class="sr-only">40% Complete (success)</span>
                      </div>
                    </div>
                    <div class="progress">
                      <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                        <span class="sr-only">20% Complete</span>
                      </div>
                    </div>
                    <div class="progress">
                      <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                        <span class="sr-only">60% Complete (warning)</span>
                      </div>
                    </div>
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        <span class="sr-only">80% Complete (danger)</span>
                      </div>
                    </div>
            </div>
            <h3></h3>
            <div class="aler_row">
                <div class="progress">
                  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                    <span class="sr-only">45% Complete</span>
                  </div>
                </div>
            </div>
            <h3>Media object</h3>
            <div class="aler_row">
                <div class="media">
                  <div class="media-left">
                    <a href="#">
                      <img class="media-object" src="images/test1.jpg" alt="...">
                    </a>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Media heading</h4>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                  </div>
                </div>
            </div>
            <h3>Panels</h3>
            <div class="aler_row">
                <div class="panel panel-default">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
                <div class="panel panel-success">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
                <div class="panel panel-warning">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
                <div class="panel panel-info">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
                <div class="panel panel-danger">
                  <div class="panel-heading">Panel heading without title</div>
                  <div class="panel-body">
                    Panel content
                  </div>
                </div>
            </div>
            <h3>With tables</h3>
            <div class="aler_row">
                <div class="panel panel-default">
                  <!-- Default panel contents -->
                  <div class="panel-heading">Panel heading</div>
                  <div class="panel-body">
                    <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                  </div>
                  <!-- Table -->
                  <table class="table">
                      <thead>
                          <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Age</th>
                              <th>mobile</th>
                              <th>sex</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>1</td>
                              <td>Nadeem Ahamad</td>
                              <td>21</td>
                              <td>987654321</td>
                              <td>male</td>
                          </tr>
                          <tr>
                              <td>2</td>
                              <td>Sujat Ahmad</td>
                              <td>25</td>
                              <td>987654321</td>
                              <td>male</td>
                          </tr>
                          <tr>
                              <td>3</td>
                              <td>Rehan</td>
                              <td>21</td>
                              <td>987654321</td>
                              <td>male</td>
                          </tr>
                      </tbody>
                  </table>
                </div>
            </div>
            <h3>Responsive embed</h3>
            <div class="aler_row">
                <div class="col-lg-6">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="images/1585aa12ce39492f.mp4"></iframe>
                </div>
                </div>
            </div>
            <h3>Wells</h3>
            <div class="aler_row">
                <div class="well">Look, I'm in a well!</div>
            </div>
            </div>
    </div>
</body>
</html>

