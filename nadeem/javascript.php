<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Learning|Javascript page</title>
    </head>
<body>
    <?php include("menu.php"); ?>
    <div class="container">
        <div class="panel" style="background-color: lightyellow">
            <h3>This first Example of jquery</h3>
            <p>If you click on me, I will disappear.</p>
            <p>Click me away!</p>
            <p>Click me too!</p>
        </div>
        <div class="panel" style="background-color: lightyellow">
            <h3>Show Hide</h3>
            <p id="pragraf">If you click on the "Hide" button, I will disappear.</p>

            <button id="hide">Hide</button>
            <button id="show">Show</button>
        </div>
        <div class="panel" style="background-color: lightyellow">
            <h3>Show Hide slow</h3>
            <p id="pragraf1">If you click on the "Hide" button, I will disappear.</p>
            
            <button id="hide2">Hide</button>
            <button id="show2">Show</button>
        </div>
        <div class="panel" style="background-color: lightyellow">
            <h3>Show Hide with toggle button</h3>
            <button id="button">Toggle between hiding and showing the paragraphs</button>

            <p id="pragraf3">This is a paragraph with little content.</p>
            <p id="pragraf4">This is another small paragraph.</p>

        </div>
        <div class="panel" style="background-color: lightyellow">
            <h3>fad in/out boxs</h3>
            <button id="btn-fadin">Click to fade in boxes</button><button id="btn-fadout">Click to fade out boxes</button><br><br>

            <div id="div1" style="width:80px;height:80px;display:none;background-color:red;"></div><br>
            <div id="div2" style="width:80px;height:80px;display:none;background-color:green;"></div><br>
            <div id="div3" style="width:80px;height:80px;display:none;background-color:blue;"></div>
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
                <p>Demonstrate fade Toggle with different speed parameters.</p>

                <button id="btn-toggle">Click to fade Toggle boxes</button><br><br>

                <div id="tdiv1" style="width:80px;height:80px;background-color:red;"></div>
                <br>
                <div id="tdiv2" style="width:80px;height:80px;background-color:green;"></div>
                <br>
                <div id="tdiv3" style="width:80px;height:80px;background-color:blue;"></div>
        </div>
        <div class="panel" style="background-color: lightyellow">
            <p>Demonstrate fadeTo() with different speed parameters.</p>

                <button id="btn-to">Click to fade To boxes</button><br><br>

                <div id="todiv1" style="width:80px;height:80px;background-color:red;"></div>
                <br>
                <div id="todiv2" style="width:80px;height:80px;background-color:green;"></div>
                <br>
                <div id="todiv3" style="width:80px;height:80px;background-color:blue;"></div>
        </div>
        <div class="panel panel-info">
                  <div class="panel-heading">click me for sliding</div>
                  <div class="panel-body">
                    Panel content
                  </div>
        </div>
        <div class="panel" style="background-color: lightyellow; position: relative;">
            <button id="btn-animate">Start Animation</button>
            <p>By default, all HTML elements have a static position, and cannot be moved. To manipulate the position, remember to first set the CSS position property of the element to relative, fixed, or absolute!</p>
            <div id="anidiv" style="background:#98bf21;height:100px;width:100px;position:absolute;"></div>
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
        <div class="panel" style="background-color: lightyellow">
            
        </div>
    </div>
    
</body>
</html>

