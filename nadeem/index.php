<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
    <head>
        <?php include("head.php"); ?>
        <title>Learning| Index page</title>
    </head>
<body>
    <?php include("menu.php"); ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="panel panel-default">
                   <div class="panel-body"><h1>Learning page</h1></div>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="https://images.pexels.com/photos/256430/pexels-photo-256430.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="...">
                                <div class="carousel-caption">
                                    https://www.pexels.com/photo/close-up-of-multi-colored-pencils-256430/
                                </div>
                            </div>
                            <div class="item">
                                <img src="https://images.pexels.com/photos/159644/art-supplies-brushes-rulers-scissors-159644.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="...">
                                <div class="carousel-caption">
                                    https://www.pexels.com/photo/pencils-in-stainless-steel-bucket-159644/
                                </div>
                            </div>
                            ...
                        </div>
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
        <h2>Image Gallery</h2>
        <p>The .thumbnail class can be used to display an image gallery.</p>
        <p>The .caption class adds proper padding and a dark grey color to text inside thumbnails.</p>
        <p>Click on the images to enlarge them.</p>
        <div class="row">
        <div class="col-md-4">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        <div class="col-md-4 ">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        <div class="col-md-4">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-4">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        <div class="col-md-4 ">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        <div class="col-md-4">
        <div class="thumbnail">
        <a href="images\test1.jpg" target="_blank">
          <img src="images\test1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
        </div>
        </div>
        </div>
    </div>
</body>
</html>